#!/bin/bash
mkdir -p ~/.cache/updates

##*===============================================
##* VARIABLE DECLARATION
##*===============================================
readonly WORK_PATH=~/.cache/updates   # Currently the log files are stored here
readonly NUM_DAYS_LOG=14              # Number of days after which old log files will be deleted

# Function: logging
# Purpose: Creates a log file for script output and manages log rotation
# Parameters: None
# Return value: None
function logging() {
  # Check if the folder already exists
  if [[ ! -d "${WORK_PATH}" ]]; then
    mkdir -p "${WORK_PATH}"
  fi

  # Redirect all output to the log file
  LOG_DATE=$(date +"%Y-%m-%d")
  exec &> >(tee -a "${WORK_PATH}/updates_info-${LOG_DATE}.log")

  # Delete old log files
  find "${WORK_PATH}" -name "updates_info-*.log" -type f -mtime +"${NUM_DAYS_LOG}" -exec rm {} \;
}

# Function: time_start
# Purpose: Records the start time of the script execution
# Parameters: None
# Return value: None
function time_start() {
  # Start date and time
  START_TIME=$(date +"%Y-%m-%d %H:%M:%S")

  # Command to write start date and time to the log file and overwrite the file
  echo "Script started at ${START_TIME}"
}

# Function: time_end
# Purpose: Calculates the script execution duration and outputs end time
# Parameters: None
# Return value: None
function time_end() {
  # End date and time
  END_TIME=$(date +"%Y-%m-%d %H:%M:%S")

  # Calculate duration in seconds
  SECONDS=$(($(date -d "${END_TIME}" +%s) - $(date -d "${START_TIME}" +%s)))

  # Extract hours, minutes, and seconds from the seconds
  HOURS=$((SECONDS / 3600))
  MINUTES=$(((SECONDS % 3600) / 60))
  SECONDS=$((SECONDS % 60))

  # Command to output end date, time, and duration
  echo "Script finished at ${END_TIME} (Duration: ${HOURS} hours ${MINUTES} minutes ${SECONDS} seconds)"
}

# Function: update_arch
# Purpose: Updating Arch or distros based on it
# Parameters: None
# Return value: None
function update_arch() {
  if command -v yay &> /dev/null; then
    if \
      yay -Syu && \
      yay -Yc
    then
      echo "Alle System-Aktualisierungen wurden durchgeführt."
    else
      echo "Ein Fehler ist bei den System-Aktualisierungen aufgetreten."
    fi
  elif command -v pamac &> /dev/null; then
    if \
      sudo pamac update && \
      sudo pamac upgrade
    then
      echo "Alle System-Aktualisierungen wurden durchgeführt."
    else
      echo "Ein Fehler ist bei den System-Aktualisierungen aufgetreten."
    fi
  else
    if \
      sudo pacman -Syu && \
      sudo pacman -Rcs "$(pacman -Qdtq)"
    then
      echo "Alle System-Aktualisierungen wurden durchgeführt."
    else
      echo "Ein Fehler ist bei den System-Aktualisierungen aufgetreten."
    fi
  fi
}

# Function: update_debian
# Purpose: Updating Debian or distros based on it
# Parameters: None
# Return value: None
function update_debian() {
  if \
    sudo apt-get update && \
    sudo apt-get upgrade -y && \
    sudo apt autoremove -y
  then
    echo "Alle System-Aktualisierungen wurden durchgeführt."
  else
    echo "Ein Fehler ist bei den System-Aktualisierungen aufgetreten."
  fi
}

# Function: update_fedora
# Purpose: Updating Fedora
# Parameters: None
# Return value: None
function update_fedora() {
  if \
    sudo dnf check-update && \
    sudo dnf update -y && \
    sudo dnf autoremove -y
  then
    echo "Alle System-Aktualisierungen wurden durchgeführt."
  else
    echo "Ein Fehler ist bei den System-Aktualisierungen aufgetreten."
  fi
}

# Function: update_opensuse
# Purpose: Updating openSUSE
# Parameters: None
# Return value: None
function update_opensuse() {
  if \
    sudo zypper dup -d && \
    sudo zypper dup
  then
    echo "Alle System-Aktualisierungen wurden durchgeführt."
  else
    echo "Ein Fehler ist bei den System-Aktualisierungen aufgetreten."
  fi
}

# Function: update_gentoo
# Purpose: Updating openSUSE
# Parameters: None
# Return value: None
function update_gentoo() {
  if \
    sudo emerge --sync && \
    sudo emerge -avuDU @world && \
    sudo emerge --depclean && \
    sudo emerge -avuDN @world
  then
    echo "Alle System-Aktualisierungen wurden durchgeführt."
  else
    echo "Ein Fehler ist bei den System-Aktualisierungen aufgetreten."
  fi
}

# Function: update_flatpak
# Purpose: Updating flatpak
# Parameters: None
# Return value: None
function update_flatpak() {
  if \
    flatpak update --noninteractive && \
    flatpak uninstall --unused --noninteractive
  then
    echo "Alle Flatpak Updates wurden erfolgreich durchgeführt."
  else
    echo "Ein Fehler ist bei den Flatpak-Aktualisierungen aufgetreten."
  fi
}

# Function: update_snap
# Purpose: Updating snap
# Parameters: None
# Return value: None
function update_snap() {
  if \
    sudo snap refresh
  then
    echo "Alle Snap-Aktualisierungen wurden durchgeführt."
  else
    echo "Ein Fehler ist bei den Snap-Aktualisierungen aufgetreten."
  fi
}

# Function: cleanup_snap
# Purpose: Remove old snap packages
# Parameters: None
# Return value: None
function cleanup_snap() {
  LANG=C snap list --all | awk '/disabled/{print $1, $3}' |
    while read -r snapname revision; do
        snap remove "$snapname" --revision="$revision"
    done
}

echo "Dieses Skript installiert Updates auf dem Computer"

logging

sudo echo "System wird ermittelt und Updates werden gestartet."

time_start

# Bestimmen der Linux-Distribution anhand der verfügbaren Update-Befehle
if command -v pacman &> /dev/null; then
  update_arch
elif command -v apt-get &> /dev/null; then
  update_debian
elif command -v dnf &> /dev/null; then
  update_fedora
elif command -v zypper &> /dev/null; then
  update_opensuse
elif command -v emerge &> /dev/null; then
  update_gentoo
else
  echo "Die Distribution wird nicht unterstützt."
fi

# Überprüfen, ob Flatpak installiert ist
if command -v flatpak &> /dev/null; then
  update_flatpak
else
    echo "Flatpak ist nicht installiert."
fi

# Überprüfen, ob Snap installiert ist
if command -v snap &> /dev/null; then
  update_snap
else
  echo "Snap ist nicht installiert."
fi

time_end