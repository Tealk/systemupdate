# System Update Script (Linux)

## Overview
This Bash script is designed to simplify the process of updating your Linux system. It identifies your Linux distribution and uses the appropriate package manager to update the system, including both package updates and cleaning up old packages. Additionally, it checks for Flatpak and Snap package managers and updates them if installed. This script is provided under the MIT License.

## Prerequisites
- Linux-based operating system (e.g., Arch, Debian, Fedora, openSUSE, Gentoo)
- Appropriate permissions to run package manager commands
- Optional: Flatpak and Snap installed (if you want to update them)

## Installation
1. Download the script to your desired location.
2. Open your terminal and navigate to the directory where the script is located.
3. Make the script executable by running the following command:
   ```
   chmod +x system_update.sh
   ```

## Usage
Execute the script with the following command:
```
./system_update.sh
```

## License
This script is distributed under the MIT License. You can find the full license text in the LICENSE file included with this script.

## Script Details
- The script will create a log file in `~/.cache/updates` to record its execution.
- It identifies your Linux distribution and uses the appropriate update commands.
- It checks for the presence of Flatpak and Snap package managers and updates them if installed.
- Old log files are periodically deleted to manage disk space.

## Supported Distributions
- Arch Linux and derivatives (e.g., Manjaro): Uses `yay` or `pamac` with `pacman` fallback.
- Debian and derivatives (e.g., Ubuntu): Uses `apt-get`.
- Fedora: Uses `dnf`.
- openSUSE: Uses `zypper`.
- Gentoo: Uses `emerge`.
- Flatpak: Updates Flatpak packages.
- Snap: Updates Snap packages and removes old ones.

## Troubleshooting
- If you encounter any issues, please check if you have the required permissions to run package manager commands.
- Ensure that the script is executed with administrative privileges if necessary.
- If you encounter problems specific to your distribution, consult the documentation or support resources for that distribution.

Feel free to customize and modify the script according to your needs.

**Note:** Use this script at your own risk. Make sure you have backups and understand the implications of updating your system.
